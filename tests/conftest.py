from datetime import datetime, timedelta

import pytest

from rest_framework.test import APIClient

from huscy.appointments.models import Appointment, Invitation, Resource, Token


@pytest.fixture
def user(django_user_model):
    return django_user_model.objects.create_user(username='user', password='password',
                                                 first_name='Lars', last_name='Krismes')


@pytest.fixture
def admin_client(admin_user):
    client = APIClient()
    client.login(username=admin_user.username, password='password')
    return client


@pytest.fixture
def client(user):
    client = APIClient()
    client.login(username=user.username, password='password')
    return client


@pytest.fixture
def anonymous_client():
    return APIClient()


@pytest.fixture
def room_1():
    return Resource.objects.create(name='room 1')


@pytest.fixture
def room_2():
    return Resource.objects.create(name='room 2')


@pytest.fixture
def appointment(user, room_1):
    return Appointment.objects.create(
        owner=user,
        resource=room_1.name,
        start=datetime.now(),
        end=datetime.now() + timedelta(hours=3))


@pytest.fixture
def appointment_second(user, room_2):
    return Appointment.objects.create(
        owner=user,
        resource=room_2.name,
        start=datetime.now() + timedelta(hours=4),
        end=datetime.now() + timedelta(hours=7))


@pytest.fixture
def invitation(appointment):
    return Invitation.objects.create(appointment=appointment, participant='participant')


@pytest.fixture
def user_feed_token(user):
    return Token.objects.create(user=user)
